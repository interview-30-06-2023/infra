# Infrastructure

This repository deploys the neccessary AWS infrastructure to support a static website. In detail, this includes an S3 bucket to store the website code and a CloudFront distribution to cache the content across multiple geographic locations and serve it as efficiently as possible.

## Usage

The infrastructure is deployed through Terraform. In order to make changes, you can push changes to any of the Terraform files. A Gitlab runner will be triggered and deploy the changes to the AWS environment. If you wish to clone or fork this repo, you need to supply AWS credentials in order to authenticate. You can do this by configuring the environment variables AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, and AWS_DEFAULT_REGION. 

A link to the live version of the website can be found in the Terraform outputs. Please check the apply job for the most recent URL.

## Improvements

- To further secure the web server, the WAF service can be configured on the CloudFront distribution. Managed rules can be applied to prevent unauthorised access from bots, crawlers, amongst other malicious activity.
- Several different environnments should be configured. The currrent deployment should be considered as production. Development, testing, and staging environments should be configured as required. The CI/CD flow should then be modified to deploy to each environment. The naming convention of each resource should be modified accordingly to include the environment.
- More generally, a branching strategy should be considered and implemented. At the moment, changes are pushed directly to master. Personally, I consider trunk based development with feature branches merging to the main branch as optimal for Terraform deployments.
- CODEOWNERS should be configured within the repository to ensure the relevant approvals are required before changes can be merged into the master branch.
- The state file should be locked to ensure parallel deployments do not happen. This can be implemented using DynamoDB backend.
- For more fine-grained access control of the state bucket, the backend can be configured to be stored within S3. Access can be granted via the KMS key used to encrypt the state file.
- Finally, cache behaviour should be modified depending upon the file type if required. If some file types are subject to change more frequently, for example images, the TTL and other parameters can be tweaked accordingly.

## Alternative solutions

- The code is seperated from the infrastructure. The code could also be included within the same repository, but there are pros and cons to both approaches. Ultimately, I opted to seperate the repositories to save time in customising the CI/CD definition provided by Gitlab. This approach also has the benefit of not triggering application deployments when only Terraform files are modified. In real world scenarios, several considerations should be made to decide whether they are separated or not. 
- If you do not require localised content via a CDN, you can skip the CloudFront distribution. Instead, you can configure static website hosting directly on S3. This would have the advantage of costing less money. In this scenario, it makes sense to cache content geographically on CloudFront since the content is static.
- One could also install a web server on an EC2 instance and deploy any code changes to the machine in order to update the website. The drawback of this approach is that you have to manage the underlying infrastucture and administrate the web server.
- Another approach is to leverage serverless solutions such as Lambda or ECS Fargate. Each of these approaches also incur additional complexity when compared to S3. For ECS, you have to build and deploy container images as well as maintain task definitions. For Lambda, you have to consider execution roles, entrypoint handlers, and programming languages to serve the HTML content.