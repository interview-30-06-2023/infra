terraform {

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.6.1"
    }
  }

  ##use gitlab state backend
  backend "http" {
  }

}

provider "aws" {
  region = "eu-west-2"

  default_tags {
    tags = {
      Environment = "Dev"
      Project     = "Interview"
    }
  }
}

data "aws_caller_identity" "current" {}

locals {
  originID = "s3"
}