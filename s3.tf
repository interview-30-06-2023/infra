# resource "aws_s3_bucket" "website" {
#   bucket_prefix = "website-"
# }

# resource "aws_s3_bucket_policy" "website" {
#   bucket = aws_s3_bucket.website.id
#   policy = data.aws_iam_policy_document.website.json
# }

# resource "aws_s3_bucket_ownership_controls" "website" {
#   bucket = aws_s3_bucket.website.id

#   rule {
#     object_ownership = "BucketOwnerEnforced"
#   }
# }

# ## Placeholder index before application CI/CD is run
# resource "aws_s3_object" "index" {
#   bucket       = aws_s3_bucket.website.id
#   key          = "index.html"
#   source       = "files/index.html"
#   etag         = filemd5("files/index.html")
#   content_type = "text/html"

#   lifecycle {
#     ignore_changes = [
#       etag
#     ]
#   }
# }

# data "aws_iam_policy_document" "website" {

#   statement {

#     effect = "Allow"

#     principals {
#       type        = "Service"
#       identifiers = ["cloudfront.amazonaws.com"]
#     }

#     actions = [
#       "s3:GetObject"
#     ]

#     resources = [
#       "${aws_s3_bucket.website.arn}/*",
#     ]

#     condition {
#       test     = "StringEquals"
#       variable = "AWS:SourceArn"

#       values = [
#         aws_cloudfront_distribution.website.arn
#       ]
#     }
#   }

# }
